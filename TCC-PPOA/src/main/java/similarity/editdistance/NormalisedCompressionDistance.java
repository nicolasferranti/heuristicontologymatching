/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.editdistance;

import java.util.zip.Deflater;

/**
 *
 * @author nicolasferranti
 */
public class NormalisedCompressionDistance implements IEditDistance {

    private float ncd(String x, String y) {
        // System.out.println("C(x+y): " + C(x+y));
        // System.out.println("C(x): " + C(x));
        // System.out.println("C(y): " + C(y));
        int cx = C(x);
        int cy = C(y);
        int cxy = C(x + y);
        return (cxy - (float) Math.min(cx, cy)) / Math.max(cx, cy);
    }

    // Find the length of a String after compression
    public int C(String inputString) {
        // Encode a String into bytes
        byte[] input = inputString.getBytes();

        // Compress the bytes
        byte[] output = new byte[input.length + 100];
        Deflater compresser = new Deflater();
        compresser.setInput(input);
        compresser.finish();

        // Return the *length* of the compressed version.
        return compresser.deflate(output);
    }

    public float similatiryNCD(String x, String y){
        return 1-ncd(x, y);
    }
    
    
    @Override
    public float compute(String s1, String s2) {
        return this.similatiryNCD(s1, s2);
    }
    
}
