/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package similarity.combination;

import similarity.common.SimilarityComparator;
import similarity.common.SimilarityVO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Isabella Pires Capobiango
 */
public class FirstMatchCombination implements ICombination {

    public Collection<SimilarityVO> explore(Collection<SimilarityVO> similaridade){
        
        //Ordenar para que as maiores similaridaes fiquem no topo
        Collections.sort((List)similaridade, new SimilarityComparator());
        
        //Copia uma lista de similaridade para poder fazer a remoção
        Collection<SimilarityVO> similaridadeAUX = new ArrayList<SimilarityVO>();
        Collection<SimilarityVO> similaridadeFINAL = new ArrayList<SimilarityVO>();
        similaridadeAUX.addAll(similaridade);


       // para cada similaridade na tabela principal 
       for (SimilarityVO similarityVO : similaridade){
        
           // se a auxiliar ja possui a similaridade
           if (similaridadeAUX.contains(similarityVO)){

               //adiciona a similaridade na final
               similaridadeFINAL.add(similarityVO);
               //Procura em todas as linhas os indicadores já combinados
               // para cada similaridade na tabela de similaridade
               for (SimilarityVO similarityVOAUX :similaridade){
                   // se a final não tem essa similaridade aux
                   if (!similaridadeFINAL.contains(similarityVOAUX)) {
                       // se os elementos da similaridade da principal são iguais remove a similaridade da tabela aux
                       if (similarityVO.getElementA() == similarityVOAUX.getElementA() || similarityVO.getElementB() == similarityVOAUX.getElementB()){
                           similaridadeAUX.remove(similarityVOAUX);
                       }
                   }
               }
           }
        }
        return similaridadeAUX;
    }
}
