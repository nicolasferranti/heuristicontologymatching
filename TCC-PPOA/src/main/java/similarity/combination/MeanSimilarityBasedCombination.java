/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.combination;

import com.hp.hpl.jena.ontology.OntResource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.HashBag;
import similarity.common.SimilarityComparator;
import similarity.common.SimilarityVO;

/**
 *
 * @author nicolasferranti
 */
public class MeanSimilarityBasedCombination implements ICombination {

   // as vezes a entrada vem desordenada e o algoritmo precisa dos ElementA's ordenados pra computar a versatilidade
   //ordenar por nome!
    private Collection<SimilarityVO> ordenaPorNome(Collection<SimilarityVO> similaridade) {
        ArrayList<SimilarityVO> simiNew = new ArrayList<>();

        OntResource o1;
        for (SimilarityVO sims : similaridade) {

            if (!simiNew.contains(sims)) {
                o1 = sims.getElementA();
                simiNew.add(sims);
                for (SimilarityVO simsAUX : similaridade) {
                    if (!simiNew.contains(simsAUX) && simsAUX.getElementA() == o1) {
                        simiNew.add(simsAUX);
                    }
                }
            }
        }
        return simiNew;
    }

    @Override
    public Collection<SimilarityVO> explore(Collection<SimilarityVO> similaridade) {

        if (similaridade.isEmpty()) {
            return similaridade;
        }

        //Ordenar para que as maiores similaridaes fiquem no topo
        Collections.sort((List) similaridade, new SimilarityComparator());

        System.out.println("");
        for (SimilarityVO sim : similaridade) {
            System.out.println(sim.getElementB() + " " + sim.getElementA() + " " + sim.getSimilarity());
        }
        similaridade = ordenaPorNome(similaridade);
        System.out.println("");
        for (SimilarityVO sim : similaridade) {
            System.out.println(sim.getElementB() + " " + sim.getElementA() + " " + sim.getSimilarity());
        }

        //mapa para armazenar a versatilidade
        Map<OntResource, Float> EntidadeVersatilidade = new HashMap<>();

        OntResource Ei = null;
        float versatilidadeEntidadeI = 0;
        int total_de_aparicoes = 0;

        
        // percorro "similaridade" fazendo a conta da versatilidade
        for (Iterator<SimilarityVO> i = similaridade.iterator(); i.hasNext();) {
            SimilarityVO triplaEaEbSimilaridade = i.next();

            // na primeira execução atribuo o valor a Ei
            if (Ei == null) {
                Ei = triplaEaEbSimilaridade.getElementA();
            }

            // se ao iterar encontrar mais Ei's, ir acumumulando sua similaridade
            if (triplaEaEbSimilaridade.getElementA() == Ei) {
                versatilidadeEntidadeI += triplaEaEbSimilaridade.getSimilarity();
                total_de_aparicoes++;
            } else {
                // encontrou alguem diferente de Ei, armazena Ei e atualiza os contadores
                EntidadeVersatilidade.put(Ei, versatilidadeEntidadeI / (float) total_de_aparicoes);

                Ei = triplaEaEbSimilaridade.getElementA();
                versatilidadeEntidadeI = triplaEaEbSimilaridade.getSimilarity();
                total_de_aparicoes = 1;
            }

            if (!i.hasNext()) {
                EntidadeVersatilidade.put(Ei, versatilidadeEntidadeI / (float) total_de_aparicoes);
                versatilidadeEntidadeI = 0;
                total_de_aparicoes = 0;
            }

        }

        Iterator it = EntidadeVersatilidade.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());

        }
        
        
        Collection<SimilarityVO> similaridadeFinal = new ArrayList<>();
        
        /*
        TODO: elencar os candidados por versatilidade
              eliminar menos aptos (?)
              selecionar os matches
        */

//        for (SimilarityVO sim : similaridade) {
//            System.out.println(sim.getElementA().getNameSpace());
//            System.out.println(sim.getElementB().getNameSpace());
//            System.out.println(sim.getSimilarity());
//        }
        return null;
    }

}
