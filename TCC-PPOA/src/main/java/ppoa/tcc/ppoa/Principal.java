/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppoa.tcc.ppoa;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

/**
 *
 * @author nicolasferranti
 */
public class Principal {

//    public static void finishSignal(){
//        try {
//            File yourFile;
//            AudioInputStream stream;
//            AudioFormat format;
//            DataLine.Info info;
//            Clip clip;
//
//            stream = AudioSystem.getAudioInputStream(new File("/var/www/html/Beep_Sound_Effect.wav"));
//            format = stream.getFormat();
//            info = new DataLine.Info(Clip.class, format);
//            clip = (Clip) AudioSystem.getLine(info);
//            clip.open(stream);
//            clip.start();
//            Thread.sleep(2500);
//        } catch (Exception e) {
//            System.out.println("um");
//            //whatevers
//        }
//    }
    public static void main(String[] args) throws Exception {

        //--> o XMLReader esta com caminhos de arquivo hardcoded
        Properties props = new Properties();
        FileInputStream file = new FileInputStream(args[0]);
        props.load(file);

        //String[] array = props.getProperty("prop.testNumbers").split(",");
        String dirBenchPath = props.getProperty("prop.dirBaseBenchmark");
        String benchPath = props.getProperty("prop.benchPath");
        String projectPath = props.getProperty("prop.projectPath");
        String printInformation = props.getProperty("prop.printInformation");
        String outputFileName = props.getProperty("prop.outputFileName");
        boolean b = printInformation.equals("true");

        String heuristica = props.getProperty("prop.optimizationApproach");
        String path_otimizador_configs = null;

        boolean otimizador0ag1ppa = false;

        switch (heuristica) {
            case "GA":
                otimizador0ag1ppa = false;
                path_otimizador_configs = props.getProperty("prop.pathGAConfig");
//                path_otimizador_configs = args[2];
                break;
            case "PPA":
                otimizador0ag1ppa = true;
                path_otimizador_configs = props.getProperty("prop.pathPPAConfig");
//                path_otimizador_configs = args[3];
                break;
            default:
                System.out.println("Otimizador nao reconhecido");
                System.exit(0);
        }

        MatcherBenchmak2016 mb = new MatcherBenchmak2016(dirBenchPath, outputFileName, projectPath, otimizador0ag1ppa, path_otimizador_configs);

        //for (String testNumber : array) {
//        int number = Integer.parseInt(args[0]);
        int number = 101;
        mb.runTest(number, benchPath + 101 + ".xml", b);
        //}

    }
}

//System.out.println("iteracao "+ i);
//            mb.runTest(101, "xml_2016/benchmark/bench101.xml", true);
//            mb.runTest(201, "xml_2016/benchmark/bench201.xml", true);
//            mb.runTest(2012, "xml_2016/benchmark/bench2012.xml", false);
//            mb.runTest(2014, "xml_2016/benchmark/bench2014.xml", false);
//            mb.runTest(2016, "xml_2016/benchmark/bench2016.xml", false);
//            mb.runTest(2018, "xml_2016/benchmark/bench2018.xml", false); //LAST EDITED
//            mb.runTest(202, "xml_2016/benchmark/bench202.xml", false);
//            mb.runTest(2022, "xml_2016/benchmark/bench2022.xml", false);
//            mb.runTest(2024, "xml_2016/benchmark/bench2024.xml", false);
//            mb.runTest(2026, "xml_2016/benchmark/bench2026.xml", false);
//            mb.runTest(2028, "xml_2016/benchmark/bench2028.xml", false);
//            mb.runTest(221, "xml_2016/benchmark/bench221.xml", true);
//            mb.runTest(222, "xml_2016/benchmark/bench222.xml", false);
//            mb.runTest(223, "xml_2016/benchmark/bench223.xml", false);
//            mb.runTest(224, "xml_2016/benchmark/bench224.xml", false);
//            mb.runTest(225, "xml_2016/benchmark/bench225.xml", false);
//            mb.runTest(228, "xml_2016/benchmark/bench228.xml", false);
//            mb.runTest(232, "xml_2016/benchmark/bench232.xml", false);
//            mb.runTest(233, "xml_2016/benchmark/bench233.xml", false);
//            mb.runTest(236, "xml_2016/benchmark/bench236.xml", false);
//            mb.runTest(237, "xml_2016/benchmark/bench237.xml", false);
//            mb.runTest(238, "xml_2016/benchmark/bench238.xml", false);
//            mb.runTest(239, "xml_2016/benchmark/bench239.xml", false);
//            mb.runTest(240, "xml_2016/benchmark/bench240.xml", false);
//            mb.runTest(241, "xml_2016/benchmark/bench241.xml", false);
//            mb.runTest(246, "xml_2016/benchmark/bench246.xml", false);
//            mb.runTest(247, "xml_2016/benchmark/bench247.xml", false);
//            mb.runTest(248, "xml_2016/benchmark/bench248.xml", false);
//            mb.runTest(2482, "xml_2016/benchmark/bench2482.xml", false);
//            mb.runTest(2484, "xml_2016/benchmark/bench2484.xml", false);
//            mb.runTest(2486, "xml_2016/benchmark/bench2486.xml", false);
//            mb.runTest(2488, "xml_2016/benchmark/bench2488.xml", false);
//            mb.runTest(249, "xml_2016/benchmark/bench249.xml", false);
//            mb.runTest(2492, "xml_2016/benchmark/bench2492.xml", false);
//            mb.runTest(2494, "xml_2016/benchmark/bench2494.xml", false);
//            mb.runTest(2496, "xml_2016/benchmark/bench2496.xml", false);
//            mb.runTest(2498, "xml_2016/benchmark/bench2498.xml", false);
//            
//            mb.runTest(250, "xml_2016/benchmark/bench250.xml", false);
//            mb.runTest(2502, "xml_2016/benchmark/bench2502.xml", false);
//            mb.runTest(2504, "xml_2016/benchmark/bench2504.xml", false);
//            mb.runTest(2506, "xml_2016/benchmark/bench2506.xml", false);
//            mb.runTest(2508, "xml_2016/benchmark/bench2508.xml", false);
//            mb.runTest(251, "xml_2016/benchmark/bench251.xml", false);
//            mb.runTest(2512, "xml_2016/benchmark/bench2512.xml", false);
//            mb.runTest(2514, "xml_2016/benchmark/bench2514.xml", false);
//            mb.runTest(2516, "xml_2016/benchmark/bench2516.xml", false);
//            mb.runTest(2518, "xml_2016/benchmark/bench2518.xml", false);
//            mb.runTest(252, "xml_2016/benchmark/bench252.xml", false);
//            mb.runTest(2522, "xml_2016/benchmark/bench2522.xml", false);
//            mb.runTest(2524, "xml_2016/benchmark/bench2524.xml", false);
//            mb.runTest(2526, "xml_2016/benchmark/bench2526.xml", false);
//            mb.runTest(2528, "xml_2016/benchmark/bench2528.xml", false);
//            mb.runTest(253, "xml_2016/benchmark/bench253.xml", false);
//            mb.runTest(2532, "xml_2016/benchmark/bench2532.xml", false);
//            mb.runTest(2534, "xml_2016/benchmark/bench2534.xml", false);
//            mb.runTest(2536, "xml_2016/benchmark/bench2536.xml", false);
//            mb.runTest(2538, "xml_2016/benchmark/bench2538.xml", false);
//            mb.runTest(254, "xml_2016/benchmark/bench254.xml", false);
//            mb.runTest(2542, "xml_2016/benchmark/bench2542.xml", false);//NICE
//            mb.runTest(2544, "xml_2016/benchmark/bench2544.xml", false);
//            mb.runTest(2546, "xml_2016/benchmark/bench2546.xml", false);
//            mb.runTest(2548, "xml_2016/benchmark/bench2548.xml", false);
//            mb.runTest(257, "xml_2016/benchmark/bench257.xml", false);
//            mb.runTest(2572, "xml_2016/benchmark/bench2572.xml", false);
//            mb.runTest(2574, "xml_2016/benchmark/bench2574.xml", false);
//            mb.runTest(2576, "xml_2016/benchmark/bench2576.xml", false);
//            mb.runTest(2578, "xml_2016/benchmark/bench2578.xml", false);
//            mb.runTest(258, "xml_2016/benchmark/bench258.xml", false);
//            mb.runTest(2582, "xml_2016/benchmark/bench2582.xml", false);
//            mb.runTest(2584, "xml_2016/benchmark/bench2584.xml", false);
//            mb.runTest(2586, "xml_2016/benchmark/bench2586.xml", false);
//            mb.runTest(2588, "xml_2016/benchmark/bench2588.xml", false);
//            mb.runTest(259, "xml_2016/benchmark/bench259.xml", false);
//            mb.runTest(2592, "xml_2016/benchmark/bench2592.xml", false);
//            mb.runTest(2594, "xml_2016/benchmark/bench2594.xml", false);
//            mb.runTest(2596, "xml_2016/benchmark/bench2596.xml", false);
//            mb.runTest(2598, "xml_2016/benchmark/bench2598.xml", false);
//            mb.runTest(260, "xml_2016/benchmark/bench260.xml", false);
//            mb.runTest(2602, "xml_2016/benchmark/bench2602.xml", false);
//            mb.runTest(2604, "xml_2016/benchmark/bench2604.xml", false); //1.0
//            mb.runTest(2606, "xml_2016/benchmark/bench2606.xml", false);
//            mb.runTest(2608, "xml_2016/benchmark/bench2608.xml", false);
//            mb.runTest(261, "xml_2016/benchmark/bench261.xml", false);
//            mb.runTest(2612, "xml_2016/benchmark/bench2612.xml", false);
//            mb.runTest(2614, "xml_2016/benchmark/bench2614.xml", false);
//            mb.runTest(2616, "xml_2016/benchmark/bench2616.xml", false);
//            mb.runTest(2618, "xml_2016/benchmark/bench2618.xml", false);
//            mb.runTest(262, "xml_2016/benchmark/bench262.xml", false);
//            mb.runTest(2622, "xml_2016/benchmark/bench2622.xml", false);
//            mb.runTest(2624, "xml_2016/benchmark/bench2624.xml", false);
//            mb.runTest(2626, "xml_2016/benchmark/bench2626.xml", false);
//            mb.runTest(2628, "xml_2016/benchmark/bench2628.xml", false);
//            mb.runTest(265, "xml_2016/benchmark/bench265.xml", false);
//            mb.runTest(266, "xml_2016/benchmark/bench266.xml", false);
//        finishSignal();
//MatcherBenchmak2016 mb = new MatcherBenchmak2016("/home/nicolasferranti/Documentos/Dissertacao/heuristicontologymatching/TCC-PPOA/xml_2011");
//mb.runTest(301, "xml_2011/benchmark/bench301.xml", true);
//mb.runTest(101, "xml/xml_benchmark_Nicolas.xml", true);
//mb.runTest(101, "xml/benchmark/bench101.xml", true);
//mb.runTest(103, "xml/benchmark/bench103.xml", true);
//mb.runTest(104, "xml/benchmark/bench104.xml", true);
//mb.runTest(201, "xml/benchmark/bench201.xml", true);
//mb.runTest(202, "xml/benchmark/bench202.xml", true);
//mb.runTest(203, "xml/benchmark/bench203.xml", true);
//mb.runTest(204, "xml/benchmark/bench204.xml", true);
//mb.runTest(205, "xml/benchmark/bench205.xml", true);
//mb.runTest(206, "xml/benchmark/bench206.xml", true);
//mb.runTest(208, "xml/benchmark/bench208.xml", true);
//mb.runTest(209, "xml/benchmark/bench209.xml", true);
//mb.runTest(210, "xml/benchmark/bench210.xml", true);
//mb.runTest(221, "xml/benchmark/bench221.xml", true);
//mb.runTest(222, "xml/benchmark/bench222.xml", true);
//mb.runTest(223, "xml/benchmark/bench223.xml", true);
//mb.runTest(224, "xml/benchmark/bench224.xml", true);
//mb.runTest(225, "xml/benchmark/bench225.xml", true);
//mb.runTest(228, "xml/benchmark/bench228.xml", true);
//mb.runTest(230, "xml/benchmark/bench230.xml", true);
//mb.runTest(232, "xml/benchmark/bench232.xml", true);
//mb.runTest(233, "xml/benchmark/bench233.xml", true);
//mb.runTest(236, "xml/benchmark/bench236.xml", true);
//mb.runTest(237, "xml/benchmark/bench237.xml", true);
//mb.runTest(238, "xml/benchmark/bench238.xml", true);
//mb.runTest(239, "xml/benchmark/bench239.xml", true);
//mb.runTest(240, "xml/benchmark/bench240.xml", true);
//mb.runTest(241, "xml/benchmark/bench241.xml", true);
//mb.runTest(246, "xml/benchmark/bench246.xml", true);
//mb.runTest(247, "xml/benchmark/bench247.xml", true);
//mb.runTest(301, "xml/benchmark/bench301.xml", true);
//mb.runTest(302, "xml/benchmark/bench302.xml", true);
//        mb.runTest(303, "xml/benchmark/bench303.xml", true);
//mb.runTest(304, "xml/benchmark/bench304.xml", true);
//        MatcherBenchmak2007 mb = new MatcherBenchmak2007("").runTest(0, xmlFuncoes, true);
//        System.out.println(mb.showPrecisionRecall(0, O1, O2, al));
//        Analyser analyser = new Analyser("/home/nicolasferranti/Documentos/TCC-Nicolas/TCC-PPOA/xml/xml_benchmark_Nicolas.xml");
//        analyser.process().imprimeTabela(new java.io.PrintWriter("/home/nicolasferranti/Documentos/TCC-Nicolas/TCC-PPOA/xml/OutTest.txt"));
//        try {
//
//            
//            ArrayList<IFuncao> equacoes = new ArrayList<IFuncao>();
//
//            //Valor da soma de cada equaÃ§Ã£o em ordem 1Âª, 2Âª, 3Âª etc...
//            final double resultadoDasEquacoes[] = {1, 1, 1};
//
//            final FuncaoComposta fc1 = new FuncaoComposta();
//            fc1.add(new FuncaoSimples(0.5));
//            fc1.add(new FuncaoSimples(0.1));
//            fc1.add(new FuncaoSimples(0.2));
//            final FuncaoComposta fc2 = new FuncaoComposta();
//            fc2.add(new FuncaoSimples(0.6));
//            fc2.add(new FuncaoSimples(0.8));
//            fc2.add(new FuncaoSimples(0.1));
//            final FuncaoComposta fc3 = new FuncaoComposta();
//            fc3.add(new FuncaoSimples(0.1));
//            fc3.add(new FuncaoSimples(0.5));
//            fc3.add(new FuncaoSimples(0.8));
//            equacoes.add(fc1);
//            equacoes.add(fc2);
//            equacoes.add(fc3);
//            
//            /*
//            //Função K1 (fc3)
//            final FuncaoSimples f1 = new FuncaoSimples(0.4);
//            final FuncaoSimples f2 = new FuncaoSimples(0.1);
//            final FuncaoSimples f3 = new FuncaoSimples(0.2);
//            final FuncaoComposta fc1 = new FuncaoComposta();
//            fc1.add(f1);
//            fc1.add(f2);
//            fc1.add(f3);
//
//            final FuncaoSimples f4 = new FuncaoSimples(0.1);
//            final FuncaoComposta fc2 = new FuncaoComposta();
//            fc2.add(fc1);
//            fc2.add(f4);
//
//            final FuncaoSimples f5 = new FuncaoSimples(0.2);
//            final FuncaoSimples f6 = new FuncaoSimples(0.4);
//            final FuncaoComposta fc3 = new FuncaoComposta();
//            fc3.add(f5);
//            fc3.add(f6);
//            fc3.add(fc2);
//
//            //Função K2 (fc32)
//            final FuncaoSimples f12 = new FuncaoSimples(0.3);
//            final FuncaoSimples f22 = new FuncaoSimples(0.2);
//            final FuncaoSimples f32 = new FuncaoSimples(0.4);
//            final FuncaoComposta fc12 = new FuncaoComposta();
//            fc12.add(f12);
//            fc12.add(f22);
//            fc12.add(f32);
//
//            final FuncaoSimples f42 = new FuncaoSimples(0.7);
//            final FuncaoComposta fc22 = new FuncaoComposta();
//            fc22.add(fc12);
//            fc22.add(f42);
//
//            final FuncaoSimples f52 = new FuncaoSimples(0.1);
//            final FuncaoSimples f62 = new FuncaoSimples(0.5);
//            final FuncaoComposta fc32 = new FuncaoComposta();
//            fc32.add(f52);
//            fc32.add(f62);
//            fc32.add(fc22);
//
//            //Função K3 (fc33)
//            final FuncaoSimples f13 = new FuncaoSimples(0.5);
//            final FuncaoSimples f23 = new FuncaoSimples(0.1);
//            final FuncaoSimples f33 = new FuncaoSimples(0.1);
//            final FuncaoComposta fc13 = new FuncaoComposta();
//            fc13.add(f13);
//            fc13.add(f23);
//            fc13.add(f33);
//
//            final FuncaoSimples f43 = new FuncaoSimples(0.4);
//            final FuncaoComposta fc23 = new FuncaoComposta();
//            fc23.add(fc13);
//            fc23.add(f43);
//
//            final FuncaoSimples f53 = new FuncaoSimples(0.7);
//            final FuncaoSimples f63 = new FuncaoSimples(0.2);
//            final FuncaoComposta fc33 = new FuncaoComposta();
//            fc33.add(f53);
//            fc33.add(f63);
//            fc33.add(fc23);
//
//            equacoes.add(fc3);
//            equacoes.add(fc32);
//            equacoes.add(fc33);
//            */
//            Gerador g = new Gerador(equacoes, resultadoDasEquacoes);
//
//            ArrayPesosGranulares apg = new ArrayPesosGranulares(0.005);
//
//            int tamPop = 60;
//
//            List<Cromossomo> pop = g.criaPopulacao(tamPop, apg);
//
//            PresaPredador pp = new PresaPredador(g, tamPop, 0.005);
//            pp.SimulaVida(1000, false);
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            //System.out.println(e.getCause());
//            e.printStackTrace();
//
//        }
//        pp.printDiferenca();
//        pp.ordenaPorFitness();
//        pp.printDiferenca();
//pp.calculaDirecao(3);
//        for (int i = 0; i < tamPop; i++) {
//            for (Iterator<Gene> it = pop.get(i).genes.iterator(); it.hasNext();) {
//                Gene ge = it.next();
//                System.out.print(ge.getValor() + " ");
//            }
//            BuscaLocal.avaliaCromossomo(pop.get(i), g);
//            System.out.println("| Difference: " + pop.get(i).getDiferenca());
//        }
//        System.out.println("--------------");
//        Cromossomo win = BuscaLocal.runGRASP2(pop, g);
//
//        for (Iterator<Gene> it = win.genes.iterator(); it.hasNext();) {
//            Gene ge = it.next();
//            System.out.print(ge.getValor() + " ");
//        }
//        BuscaLocal.avaliaCromossomo(win, g);
//        System.out.println("| Difference: " + win.getDiferenca());

