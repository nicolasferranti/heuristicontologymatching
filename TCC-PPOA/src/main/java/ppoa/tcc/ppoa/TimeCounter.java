/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppoa.tcc.ppoa;

/**
 *
 * @author mazzoco
 */
public class TimeCounter {
    private static long start;
    private static long elapsed;
    
    public void iniciarContagem(){
        start = System.currentTimeMillis();
    }
    
    public void terminarContagem(){
        elapsed = System.currentTimeMillis() - start;
    }
    
    @Override
    public String toString(){
        float x = (float)elapsed/1000;
        String s = String.valueOf(x);
        return s;
    }
}
