/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppoa.tcc.resultStatistics;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author nicolasferranti
 */
public class Statistics {
    ArrayList<Double> data;
    int size;

    public Statistics(ArrayList<Double> data) {
        this.data = data;
        size = data.size();
    } 
    
    public double getMean() {
        double sum = 0.0;
        for (double a : data) {
            sum += a;
        }
        return sum / size;
    }

    private double getVariance() {
        double mean = getMean();
        double temp = 0;
        for (double a : data) {
            temp += (a - mean) * (a - mean);
        }
        return temp / (size - 1);
    }

    public double getStdDev() {
        return Math.sqrt(getVariance());
    }

}
