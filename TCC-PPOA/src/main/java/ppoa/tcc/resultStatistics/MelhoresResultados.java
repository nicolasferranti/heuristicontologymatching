/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppoa.tcc.resultStatistics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import static ppoa.tcc.resultStatistics.MediaDesvio.round;

/**
 *
 * @author nicolasferranti
 */
public class MelhoresResultados {

    public static void main(String[] args) throws IOException {
        String[] pastas = {"101",
            "201", "2012", "2014", "2016", "2018", "202", "2022", "2024", "2026", "2028", "221", "222", "223", "224", "225", "228", "232", "233", "236", "237", "238", "239", "240", "241", "246", "247", "248", "2482", "2484", "2486", "2488", "249", "2492", "2494", "2496", "2498", "250",
            "2502", "2504", "2506", "2508", "251", "2512", "2514", "2516", "2518", "252", "2522", "2524", "2526", "2528", "253", "2532", "2534", "2536", "2538", "254", "2542", "2544", "2546", "2548", "257", "2572", "2574", "2576", "2578", "258", "2582", "2584", "2586", "2588", "259",
            "2592", "2594", "2596", "2598", "260", "2602", "2604", "2606", "2608", "261", "2612", "2614", "2616", "2618", "262", "2622", "2624", "2626", "2628", "265", "266"};
        //String path = "/home/nicolasferranti/Documentos/Dissertacao/heuristicontologymatching/TCC-PPOA/xml_2016/";
        String path = "/home/heuristicontologymatching/TCC-PPOA/xml_2016/";

        System.out.println("\t" + "Precision" + "\t" + "Recall" + "\t" + "Best fmeasure");

        double precision;
        double recall;
        double fmeasure;

        for (int i = 0; i < pastas.length; i++) {
            precision = 0;
            recall = 0;
            fmeasure = 0;

            File f = new File(path + pastas[i] + "/tppa-p20-g100.properties");
            LineIterator it = FileUtils.lineIterator(f, "UTF-8");
            boolean first = true;
            try {
                while (it.hasNext()) {
                    String line = it.nextLine();
                    if (!first) {
                        String[] colunas = line.split(",");
                        if (fmeasure < Double.parseDouble(colunas[2])) {
                            precision = Double.parseDouble(colunas[0]);
                            recall = Double.parseDouble(colunas[1]);
                            fmeasure = Double.parseDouble(colunas[2]);
                        }

                    } else {
                        first = false;
                    }
                }
            } finally {
                it.close();
                System.out.println(pastas[i] + "\t" + round(precision, 3) + "\t" + round(recall, 3) + "\t" + round(fmeasure, 3));
            }
        }
    }
}
