/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppoa.tcc.resultStatistics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;

import org.apache.commons.io.LineIterator;

/**
 *
 * @author nicolasferranti
 */
public class MediaDesvio {

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static void main(String[] args) throws IOException {
        String[] pastas = {"101",
            "201", "2012", "2014", "2016", "2018", "202", "2022", "2024", "2026", "2028", "221", "222", "223", "224", "225", "228", "232", "233", "236", "237", "238", "239", "240", "241", "246", "247", "248", "2482", "2484", "2486", "2488", "249", "2492", "2494", "2496", "2498", "250",
            "2502", "2504", "2506", "2508", "251", "2512", "2514", "2516", "2518", "252", "2522", "2524", "2526", "2528", "253", "2532", "2534", "2536", "2538", "254", "2542", "2544", "2546", "2548", "257", "2572", "2574", "2576", "2578", "258", "2582", "2584", "2586", "2588", "259",
            "2592", "2594", "2596", "2598", "260", "2602", "2604", "2606", "2608", "261", "2612", "2614", "2616", "2618", "262", "2622", "2624", "2626", "2628", "265", "266"};
        String path = "/home/nicolasferranti/Documentos/Dissertacao/heuristicontologymatching/TCC-PPOA/xml_2016/";

        System.out.println("\t" + "Mean Prec" + "\t" + "Sdv Prec" + "\t" + "Mean Rec" + "\t" + "Sdv Rec" + "\t" + "Mean Fmeas" + "\t" + "Sdv Fmeas");

        ArrayList<Double> precision;
        ArrayList<Double> recall;
        ArrayList<Double> fmeasure;

        for (int i = 0; i < pastas.length; i++) {
            precision = new ArrayList<>();
            recall = new ArrayList<>();
            fmeasure = new ArrayList<>();

            File f = new File(path + pastas[i] + "/PrecRecLogAG.csv");
            LineIterator it = FileUtils.lineIterator(f, "UTF-8");
            boolean first = true;
            try {
                while (it.hasNext()) {
                    String line = it.nextLine();
                    if (!first) {
                        String[] colunas = line.split(",");
                        precision.add(Double.parseDouble(colunas[0]));
                        recall.add(Double.parseDouble(colunas[1]));
                        fmeasure.add(Double.parseDouble(colunas[2]));
                    } else {
                        first = false;
                    }
                }
            } finally {
                it.close();
                Statistics sPrec = new Statistics(precision);
                Statistics sRec = new Statistics(recall);
                Statistics sFm = new Statistics(fmeasure);
                System.out.println(pastas[i] + "\t" + round(sPrec.getMean(), 3) + "\t" + round(sPrec.getStdDev(), 3) + "\t" + round(sRec.getMean(), 3) + "\t" + round(sRec.getStdDev(), 3) + "\t" + round(sFm.getMean(), 3) + "\t" + round(sFm.getStdDev(), 3));
            }
        }

    }
}
