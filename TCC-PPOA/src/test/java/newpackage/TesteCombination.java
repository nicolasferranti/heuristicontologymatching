/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.hp.hpl.jena.ontology.OntResource;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.JDOMException;
import parser.XMLReader;
import similarity.combination.MeanSimilarityBasedCombination;
import similarity.common.FunctionWeightVO;
import similarity.common.SimilarityVO;
import similarity.exception.WeightException;

/**
 *
 * @author nicolasferranti
 */
public class TesteCombination {

    public static void main(String[] args) {

        try {
            XMLReader xml = new XMLReader("xml_2016/benchmark/bench" + "202" + ".xml", "/home/nicolasferranti/Documentos/Dissertacao/heuristicontologymatching/TCC-PPOA/");

            Collection<SimilarityVO> similaridade = new ArrayList<SimilarityVO>();

            Collection<List<OntResource>> conceitos = xml.getOntology();
            Iterator i = conceitos.iterator();

            List<OntResource> conceitos1 = (List<OntResource>) i.next();
            List<OntResource> conceitos2 = (List<OntResource>) i.next();

            for (int k = 0; k < 3; k++) {
                for (int j = 0; j < 3; j++) {

                    similaridade.add(new SimilarityVO(conceitos1.get(k), conceitos2.get(j), (float) Math.random()));

                }
            }

            for (SimilarityVO sim : similaridade) {
                System.out.println(sim.getElementB() + " " + sim.getElementA() + " " + sim.getSimilarity());
            }

            new MeanSimilarityBasedCombination().explore(similaridade);
//            for (List<OntResource> L : functions) {
//                System.out.println("Lista:");
//                for (OntResource o : L) {
//                    System.out.println(o);
//                }
//            }

        } catch (WeightException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(TesteCombination.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
