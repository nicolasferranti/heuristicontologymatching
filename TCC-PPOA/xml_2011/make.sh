#!/bin/sh
# Beware, this works when applying $ sh make.sh from csh!
# $Id: make.sh,v 1.38 2011/06/05 21:10:53 euzenat Exp $
# XSLT based test generation.
# //pass1: generate test files
# //pass2: fix URI
# //pass3: generate HTML
# //pass4: put everything on the web site (to be processed manually)

VERSION=81
CURRENT=2011/benchmarks
#CURRENT=tests

echo Generating files

# //pass1: generate test files

# NEVER DESTROY THE 3xx!
#/bin/rm -rf [1-2][0-9][0-9]
#/bin/rm -rf [1-2][0-9][0-9]-[0-9]

#####################################################################
# 101) Concept test: Id

#DONE!
/bin/rm -rf 101
mkdir 101
cp onto.rdf 101/
chmod 664 101/onto.rdf
cp refalign.rdf 101/
chmod 664 101/refalign.rdf

#####################################################################
# 102) Concept test: ?
# JUST TO BE COPIED ONCE

#####################################################################
# 103) Concept test: Language generalisation

#DONE!
/bin/rm -rf 103
mkdir 103
xsltproc xslt/generalization.xsl onto.rdf > 103/onto.rdf
cp 101/refalign.rdf 103/refalign.rdf

#####################################################################
# 104) Concept test: Language restriction

#DONE
\rm -rf 104
mkdir 104
xsltproc xslt/restriction.xsl onto.rdf > 104/onto.rdf
cp 101/refalign.rdf 104/refalign.rdf

#####################################################################
# 201) Systematic: No names
#Each label or identifier is replaced by a random one.

#DONE!
\rm -rf 201
mkdir 201
xsltproc xslt/trans-random.xsl onto.rdf > 201/onto.rdf
xsltproc xslt/trans-random.xsl refalign.rdf > 201/refalign.rdf
\rm -rf 201-2
mkdir 201-2
xsltproc xslt/trans-random2.xsl onto.rdf > 201-2/onto.rdf
xsltproc xslt/trans-random2.xsl refalign.rdf > 201-2/refalign.rdf
\rm -rf 201-4
mkdir 201-4
xsltproc xslt/trans-random4.xsl onto.rdf > 201-4/onto.rdf
xsltproc xslt/trans-random4.xsl refalign.rdf > 201-4/refalign.rdf
\rm -rf 201-6
mkdir 201-6
xsltproc xslt/trans-random6.xsl onto.rdf > 201-6/onto.rdf
xsltproc xslt/trans-random6.xsl refalign.rdf > 201-6/refalign.rdf
\rm -rf 201-8
mkdir 201-8
xsltproc xslt/trans-random8.xsl onto.rdf > 201-8/onto.rdf
xsltproc xslt/trans-random8.xsl refalign.rdf > 201-8/refalign.rdf

#####################################################################
# 202) Systematic: No names, no comment
#Each label or identifier is replaced by a random one. Comments  (rdfs:comment and dc:description) have been suppressed as well.

#DONE!
\rm -rf 202
mkdir 202
xsltproc xslt/strip-comments.xsl 201/onto.rdf > 202/onto.rdf
xsltproc xslt/trans-random.xsl refalign.rdf > 202/refalign.rdf
\rm -rf 202-2
mkdir 202-2
xsltproc xslt/strip-comments.xsl 201-2/onto.rdf > 202-2/onto.rdf
xsltproc xslt/trans-random2.xsl refalign.rdf > 202-2/refalign.rdf
\rm -rf 202-4
mkdir 202-4
xsltproc xslt/strip-comments.xsl 201-4/onto.rdf > 202-4/onto.rdf
xsltproc xslt/trans-random4.xsl refalign.rdf > 202-4/refalign.rdf
\rm -rf 202-6
mkdir 202-6
xsltproc xslt/strip-comments.xsl 201-6/onto.rdf > 202-6/onto.rdf
xsltproc xslt/trans-random6.xsl refalign.rdf > 202-6/refalign.rdf
\rm -rf 202-8
mkdir 202-8
xsltproc xslt/strip-comments.xsl 201-8/onto.rdf > 202-8/onto.rdf
xsltproc xslt/trans-random8.xsl refalign.rdf > 202-8/refalign.rdf

#####################################################################
# 203) Systematic: No comment (was Misspelling)

#DONE!
\rm -rf 203
mkdir 203
xsltproc xslt/strip-comments.xsl onto.rdf > 203/onto.rdf
cp 101/refalign.rdf 203/refalign.rdf

#####################################################################
# 204) Systematic: Naming conventions
#Different naming conventions (Uppercasing, underscore, dash, etc.)  are used for labels. Comments have been suppressed.

#DONE!
\rm -rf 204
mkdir 204
xsltproc xslt/trans-conv.xsl onto.rdf > 204/onto.rdf
xsltproc xslt/trans-conv.xsl refalign.rdf > 204/refalign.rdf

#####################################################################
# 205) Systematic: Synonyms
# Labels are replaced by synonyms. Comments have been suppressed.

#DONE!
\rm -rf 205
mkdir 205
xsltproc xslt/trans-synonyms.xsl onto.rdf > 205/onto.rdf
xsltproc xslt/trans-synonyms.xsl refalign.rdf > 205/refalign.rdf

#####################################################################
# 206) Systematic: Foreign names (including comments)
#The complete ontology is translated to another language than  english (French in the current case, but other languages would be fine).

#DONE! // NOMORECOMMENT TRANSLATIONS
\rm -rf 206
mkdir 206
xsltproc xslt/trans-foreign.xsl onto.rdf > 206/onto-iso8859.rdf
xsltproc xslt/trans-foreign.xsl refalign.rdf > 206/refalign-iso8859.rdf
cat 206/onto-iso8859.rdf | ./convert_iso2utf8.pl | sed "s:iso-8859-1:utf-8:"> 206/onto.rdf
cat 206/refalign-iso8859.rdf | ./convert_iso2utf8.pl | sed "s:iso-8859-1:utf-8:"> 206/refalign.rdf

#####################################################################
# 207) Systematic: Foreign names
#The complete ontology is translated to another language than  english (French in the current case, but other languages would be fine).

#DONE!
\rm -rf 207
mkdir 207
xsltproc xslt/trans-foreign.xsl onto.rdf > 207/onto-iso8859.rdf
cat 207/onto-iso8859.rdf | ./convert_iso2utf8.pl | sed "s:iso-8859-1:utf-8:"> 207/onto.rdf
cp 206/refalign.rdf 207/refalign.rdf
cp 206/refalign-iso8859.rdf 207/refalign-iso8859.rdf

#####################################################################
# 208) Systematic: Convention and no comments

#DONE!
\rm -rf 208
mkdir 208
xsltproc xslt/strip-comments.xsl 204/onto.rdf > 208/onto.rdf
cp 204/refalign.rdf 208/refalign.rdf

#####################################################################
# 209) Systematic: Synonyms and no comments

#DONE!
\rm -rf 209
mkdir 209
xsltproc xslt/strip-comments.xsl 205/onto.rdf > 209/onto.rdf
cp 205/refalign.rdf 209/refalign.rdf

#####################################################################
# 210) Systematic: Foreign language and no comments

#DONE!
\rm -rf 210
mkdir 210
xsltproc xslt/strip-comments.xsl 207/onto.rdf > 210/onto-iso8859.rdf
cat 210/onto-iso8859.rdf | ./convert_iso2utf8.pl | sed "s:iso-8859-1:utf-8:"> 210/onto.rdf
cp 207/refalign.rdf 210/refalign.rdf
cp 207/refalign-iso8859.rdf 210/refalign-iso8859.rdf

#####################################################################
# 221) Systematic: No hierarchy
#All subclass assertions to named classes are suppressed.
#(variation: compile inheritance)

#DONE!
\rm -rf 221
mkdir 221
xsltproc xslt/strip-subclass.xsl onto.rdf > 221/onto.rdf
cp 101/refalign.rdf 221/refalign.rdf

#####################################################################
# 222) Systematic: Flattened hierarchy
#A hierarchy still exists but has been strictly reduced.
# Suppressed Book, Part, Academic and Informal
# Replaced their apperance by Reference (incl. suppressed unionOf)
#inherited their properties

#DONE!
\rm -rf 222
mkdir 222
cp onto-flat.rdf 222/onto.rdf
chmod 644 222/onto.rdf
xsltproc xslt/flatten.xsl refalign.rdf > 222/refalign.rdf

#####################################################################
# 223) Systematic: Expanded hierarchy
#Numerous intermediate classes are introduced within the hierarchy.

#DONE!
\rm -rf 223
mkdir 223
cp onto-exp.rdf 223/onto.rdf
chmod 644 223/onto.rdf
cp 101/refalign.rdf 223/refalign.rdf

#####################################################################
# 224) Systematic: No instances
#All individuals have been suppressed from the ontology.

# DONE!
\rm -rf 224
mkdir 224
xsltproc xslt/strip-instances.xsl onto.rdf > 224/onto.rdf
cp 101/refalign.rdf 224/refalign.rdf

#####################################################################
# 225) Systematic: No restrictions
#All local restrictions on properties have been suppressed from the ontology.
#(variation: no property nor global restrictions on properties)

# DONE!
\rm -rf 225
mkdir 225
xsltproc xslt/strip-restrictions.xsl onto.rdf > 225/onto.rdf
cp 101/refalign.rdf 225/refalign.rdf

# 226) Systematic: No datatypes
#NOTTODO

# 227) Systematic: Unit differences
#NOTTODO

#####################################################################
# 228) Systematic: No properties
#Properties and relations between objects have been completely suppressed.
#(variation: leave the properties in instances)

#DONE!
\rm -rf 228
mkdir 228
xsltproc xslt/strip-properties.xsl onto.rdf > 228/onto.rdf
xsltproc xslt/strip-propalign.xsl refalign.rdf > 228/refalign.rdf

# 229) Systematic: Class vs instances
#NOTTODO

#####################################################################
# 230) Systematic: Flattening entities
#Some components of classes are expanded in the class structure  (e.g., year, month, day attributes instead of date).
#Here one limitation of the proposed format is that it does not cover  alignments such as: journalName = name o journal.

\rm -rf 230
mkdir 230
cp onto-cflat.rdf 230/onto.rdf
chmod 644 230/onto.rdf
cp refalign-cflat.rdf 230/refalign.rdf
chmod 644 230/refalign.rdf

#//TODO
# 231) Systematic: Multiplying entities
#Some classes are spreaded over several classes.
#Not available in this test.
\rm -rf 231
mkdir 231
cp 101/onto.rdf 231/onto.rdf
cp 101/refalign.rdf 231/refalign.rdf

#####################################################################
# 232) Systematic: No instances, No hierarchy

# DONE!
\rm -rf 232
mkdir 232
xsltproc xslt/strip-instances.xsl 221/onto.rdf > 232/onto.rdf
cp 221/refalign.rdf 232/refalign.rdf

#####################################################################
# 233) Systematic: No property, No hierarchy

# DONE!
\rm -rf 233
mkdir 233
xsltproc xslt/strip-properties.xsl 221/onto.rdf > 233/onto.rdf
xsltproc xslt/strip-propalign.xsl 221/refalign.rdf > 233/refalign.rdf

#####################################################################
# 234) Systematic: No hierarchy, flattened classes

# DONE!
\rm -rf 234
#mkdir 234
#xsltproc xslt/strip-subclass.xsl 230/onto.rdf > 234/onto.rdf
#cp 230/refalign.rdf 234/refalign.rdf

#####################################################################
# 235) Systematic: No hierarchy, expansed classes

# DONE!
\rm -rf 235
#mkdir 235
#xsltproc xslt/strip-subclass.xsl 231/onto.rdf > 235/onto.rdf
#cp 231/refalign.rdf 235/refalign.rdf

######################################################################
# 236) Systematic: No instances, No properties

#DONE!
\rm -rf 236
mkdir 236
xsltproc xslt/strip-properties.xsl 224/onto.rdf > 236/onto.rdf
xsltproc xslt/strip-propalign.xsl 224/refalign.rdf > 236/refalign.rdf

#####################################################################
# 237) Systematic: No instances, flattened classes

# DONE!
\rm -rf 237
mkdir 237
xsltproc xslt/strip-instances.xsl 222/onto.rdf > 237/onto.rdf
cp 222/refalign.rdf 237/refalign.rdf

#####################################################################
# 238) Systematic: No instances, expansed classes

# DONE!
\rm -rf 238
mkdir 238
xsltproc xslt/strip-instances.xsl 223/onto.rdf > 238/onto.rdf
cp 223/refalign.rdf 238/refalign.rdf

#####################################################################
# 239) Systematic: No properties, flattened classes

#DONE!
\rm -rf 239
mkdir 239
xsltproc xslt/strip-properties.xsl 222/onto.rdf > 239/onto.rdf
xsltproc xslt/strip-propalign.xsl 222/refalign.rdf > 239/refalign.rdf

#####################################################################
# 240) Systematic: No properties, expansed classes

#DONE!
\rm -rf 240
mkdir 240
xsltproc xslt/strip-properties.xsl 223/onto.rdf > 240/onto.rdf
xsltproc xslt/strip-propalign.xsl 223/refalign.rdf > 240/refalign.rdf

######################################################################
# 241) Systematic: No instances, No hierarchy, No properties

# DONE!
\rm -rf 241
mkdir 241
xsltproc xslt/strip-instances.xsl 233/onto.rdf > 241/onto.rdf
cp 233/refalign.rdf 241/refalign.rdf

#####################################################################
# 242) Systematic: No instances, No hierarchy, flattened classes

# DONE!
#\rm -rf 242
#mkdir 242
#xsltproc xslt/strip-instances.xsl 234/onto.rdf > 242/onto.rdf
#cp 234/refalign.rdf 242/refalign.rdf

#####################################################################
# 243) Systematic: No instances, No hierarchy, expansed classes

# DONE!
#\rm -rf 243
#mkdir 243
#xsltproc xslt/strip-instances.xsl 235/onto.rdf > 243/onto.rdf
#cp 235/refalign.rdf 243/refalign.rdf

######################################################################
# 244) Systematic: No properties, No hierarchy, flattened classes

#DONE!
#\rm -rf 244
#mkdir 244
#xsltproc xslt/strip-properties.xsl 234/onto.rdf > 244/onto.rdf
#xsltproc xslt/strip-propalign.xsl 234/refalign.rdf > 244/refalign.rdf


#####################################################################
# 245) Systematic: No properties, No hierarchy, expansed classes

#DONE!
#\rm -rf 245
#mkdir 245
#xsltproc xslt/strip-properties.xsl 235/onto.rdf > 245/onto.rdf
#xsltproc xslt/strip-propalign.xsl 235/refalign.rdf > 245/refalign.rdf

######################################################################
# 246) Systematic: No properties, No instances, flattened classes

#DONE!
\rm -rf 246
mkdir 246
xsltproc xslt/strip-properties.xsl 237/onto.rdf > 246/onto.rdf
xsltproc xslt/strip-propalign.xsl 237/refalign.rdf > 246/refalign.rdf

#####################################################################
# 247) Systematic: No properties, No instances, expansed classes

#DONE!
\rm -rf 247
mkdir 247
xsltproc xslt/strip-properties.xsl 238/onto.rdf > 247/onto.rdf
xsltproc xslt/strip-propalign.xsl 238/refalign.rdf > 247/refalign.rdf

####################################################################
# 248) Systematic: No names, no hierarchy

# DONE
\rm -rf 248
mkdir 248
xsltproc xslt/strip-subclass.xsl 202/onto.rdf > 248/onto.rdf
cp 202/refalign.rdf 248/refalign.rdf
\rm -rf 248-2
mkdir 248-2
xsltproc xslt/strip-subclass.xsl 202-2/onto.rdf > 248-2/onto.rdf
cp 202-2/refalign.rdf 248-2/refalign.rdf
\rm -rf 248-4
mkdir 248-4
xsltproc xslt/strip-subclass.xsl 202-4/onto.rdf > 248-4/onto.rdf
cp 202-4/refalign.rdf 248-4/refalign.rdf
\rm -rf 248-6
mkdir 248-6
xsltproc xslt/strip-subclass.xsl 202-6/onto.rdf > 248-6/onto.rdf
cp 202-6/refalign.rdf 248-6/refalign.rdf
\rm -rf 248-8
mkdir 248-8
xsltproc xslt/strip-subclass.xsl 202-8/onto.rdf > 248-8/onto.rdf
cp 202-8/refalign.rdf 248-8/refalign.rdf

#####################################################################
# 249) Systematic: No names, no instances

# DONE
\rm -rf 249
mkdir 249
xsltproc xslt/strip-instances.xsl 202/onto.rdf > 249/onto.rdf
cp 202/refalign.rdf 249/refalign.rdf
\rm -rf 249-2
mkdir 249-2
xsltproc xslt/strip-instances.xsl 202-2/onto.rdf > 249-2/onto.rdf
cp 202-2/refalign.rdf 249-2/refalign.rdf
\rm -rf 249-4
mkdir 249-4
xsltproc xslt/strip-instances.xsl 202-4/onto.rdf > 249-4/onto.rdf
cp 202-4/refalign.rdf 249-4/refalign.rdf
\rm -rf 249-6
mkdir 249-6
xsltproc xslt/strip-instances.xsl 202-6/onto.rdf > 249-6/onto.rdf
cp 202-6/refalign.rdf 249-6/refalign.rdf
\rm -rf 249-8
mkdir 249-8
xsltproc xslt/strip-instances.xsl 202-8/onto.rdf > 249-8/onto.rdf
cp 202-8/refalign.rdf 249-8/refalign.rdf

#####################################################################
# 250) Systematic: No names, no properties

#DONE!
\rm -rf 250
mkdir 250
xsltproc xslt/strip-properties.xsl 202/onto.rdf > 250/onto.rdf
xsltproc xslt/strip-propalign.xsl 202/refalign.rdf > 250/refalign.rdf
\rm -rf 250-2
mkdir 250-2
xsltproc xslt/strip-properties.xsl 202-2/onto.rdf > 250-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 202-2/refalign.rdf > 250-2/refalign.rdf
\rm -rf 250-4
mkdir 250-4
xsltproc xslt/strip-properties.xsl 202-4/onto.rdf > 250-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 202-4/refalign.rdf > 250-4/refalign.rdf
\rm -rf 250-6
mkdir 250-6
xsltproc xslt/strip-properties.xsl 202-6/onto.rdf > 250-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 202-6/refalign.rdf > 250-6/refalign.rdf
\rm -rf 250-8
mkdir 250-8
xsltproc xslt/strip-properties.xsl 202-8/onto.rdf > 250-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 202-8/refalign.rdf > 250-8/refalign.rdf

#####################################################################
# 251) Systematic: No names, flattened classes

#DONE
\rm -rf 251
mkdir 251
xsltproc xslt/strip-comments.xsl 222/onto.rdf > 251/preonto.rdf
xsltproc xslt/trans-random.xsl 251/preonto.rdf > 251/onto.rdf
xsltproc xslt/trans-random.xsl 222/refalign.rdf > 251/refalign.rdf
\rm -rf 251-2
mkdir 251-2
xsltproc xslt/strip-comments.xsl 222/onto.rdf > 251-2/preonto.rdf
xsltproc xslt/trans-random2.xsl 251-2/preonto.rdf > 251-2/onto.rdf
xsltproc xslt/trans-random2.xsl 222/refalign.rdf > 251-2/refalign.rdf
\rm -rf 251-4
mkdir 251-4
xsltproc xslt/strip-comments.xsl 222/onto.rdf > 251-4/preonto.rdf
xsltproc xslt/trans-random4.xsl 251-4/preonto.rdf > 251-4/onto.rdf
xsltproc xslt/trans-random4.xsl 222/refalign.rdf > 251-4/refalign.rdf
\rm -rf 251-6
mkdir 251-6
xsltproc xslt/strip-comments.xsl 222/onto.rdf > 251-6/preonto.rdf
xsltproc xslt/trans-random6.xsl 251-6/preonto.rdf > 251-6/onto.rdf
xsltproc xslt/trans-random6.xsl 222/refalign.rdf > 251-6/refalign.rdf
\rm -rf 251-8
mkdir 251-8
xsltproc xslt/strip-comments.xsl 222/onto.rdf > 251-8/preonto.rdf
xsltproc xslt/trans-random8.xsl 251-8/preonto.rdf > 251-8/onto.rdf
xsltproc xslt/trans-random8.xsl 222/refalign.rdf > 251-8/refalign.rdf

#####################################################################
# 252) Systematic: No names, expanded classes

#DONE
\rm -rf 252
mkdir 252
xsltproc xslt/strip-comments.xsl 223/onto.rdf > 252/preonto.rdf
xsltproc xslt/trans-random.xsl 252/preonto.rdf > 252/onto.rdf
xsltproc xslt/trans-random.xsl 223/refalign.rdf > 252/refalign.rdf
\rm -rf 252-2
mkdir 252-2
xsltproc xslt/strip-comments.xsl 223/onto.rdf > 252-2/preonto.rdf
xsltproc xslt/trans-random2.xsl 252-2/preonto.rdf > 252-2/onto.rdf
xsltproc xslt/trans-random2.xsl 223/refalign.rdf > 252-2/refalign.rdf
\rm -rf 252-4
mkdir 252-4
xsltproc xslt/strip-comments.xsl 223/onto.rdf > 252-4/preonto.rdf
xsltproc xslt/trans-random2.xsl 252-4/preonto.rdf > 252-4/onto.rdf
xsltproc xslt/trans-random2.xsl 223/refalign.rdf > 252-4/refalign.rdf
\rm -rf 252-6
mkdir 252-6
xsltproc xslt/strip-comments.xsl 223/onto.rdf > 252-6/preonto.rdf
xsltproc xslt/trans-random2.xsl 252-6/preonto.rdf > 252-6/onto.rdf
xsltproc xslt/trans-random2.xsl 223/refalign.rdf > 252-6/refalign.rdf
\rm -rf 252-8
mkdir 252-8
xsltproc xslt/strip-comments.xsl 223/onto.rdf > 252-8/preonto.rdf
xsltproc xslt/trans-random2.xsl 252-8/preonto.rdf > 252-8/onto.rdf
xsltproc xslt/trans-random2.xsl 223/refalign.rdf > 252-8/refalign.rdf

#####################################################################
# 253) Systematic: No names, no hierarchy, no instance

#DONE!
\rm -rf 253
mkdir 253
xsltproc xslt/strip-instances.xsl 248/onto.rdf > 253/onto.rdf
cp 248/refalign.rdf 253/refalign.rdf
\rm -rf 253-2
mkdir 253-2
xsltproc xslt/strip-instances.xsl 248-2/onto.rdf > 253-2/onto.rdf
cp 248-2/refalign.rdf 253-2/refalign.rdf
\rm -rf 253-4
mkdir 253-4
xsltproc xslt/strip-instances.xsl 248-4/onto.rdf > 253-4/onto.rdf
cp 248-4/refalign.rdf 253-4/refalign.rdf
\rm -rf 253-6
mkdir 253-6
xsltproc xslt/strip-instances.xsl 248-6/onto.rdf > 253-6/onto.rdf
cp 248-6/refalign.rdf 253-6/refalign.rdf
\rm -rf 253-8
mkdir 253-8
xsltproc xslt/strip-instances.xsl 248-8/onto.rdf > 253-8/onto.rdf
cp 248-8/refalign.rdf 253-8/refalign.rdf

#####################################################################
# 254) Systematic: No names, no hierarchy, no properties

#DONE
\rm -rf 254
mkdir 254
xsltproc xslt/strip-properties.xsl 248/onto.rdf > 254/onto.rdf
xsltproc xslt/strip-propalign.xsl 248/refalign.rdf > 254/refalign.rdf
\rm -rf 254-2
mkdir 254-2
xsltproc xslt/strip-properties.xsl 248-2/onto.rdf > 254-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 248-2/refalign.rdf > 254-2/refalign.rdf
\rm -rf 254-4
mkdir 254-4
xsltproc xslt/strip-properties.xsl 248-4/onto.rdf > 254-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 248-4/refalign.rdf > 254-4/refalign.rdf
\rm -rf 254-6
mkdir 254-6
xsltproc xslt/strip-properties.xsl 248-6/onto.rdf > 254-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 248-6/refalign.rdf > 254-6/refalign.rdf
\rm -rf 254-8
mkdir 254-8
xsltproc xslt/strip-properties.xsl 248-8/onto.rdf > 254-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 248-8/refalign.rdf > 254-8/refalign.rdf

#####################################################################
# 255) Systematic: No names, No hierarchy, flattened classes

#DONE
#\rm -rf 255
#mkdir 255
#xsltproc xslt/strip-subclass.xsl 251/onto.rdf > 255/onto.rdf
#cp 251/refalign.rdf 255/refalign.rdf

#####################################################################
# 256) Systematic: No names, No hierarchy, expanded classes

#DONE
#\rm -rf 256
#mkdir 256
#xsltproc xslt/strip-subclass.xsl 252/onto.rdf > 256/onto.rdf
#cp 252/refalign.rdf 256/refalign.rdf

#####################################################################
# 257) Systematic: No names, no instances, no properties

#DONE
\rm -rf 257
mkdir 257
xsltproc xslt/strip-properties.xsl 249/onto.rdf > 257/onto.rdf
xsltproc xslt/strip-propalign.xsl 249/refalign.rdf > 257/refalign.rdf
\rm -rf 257-2
mkdir 257-2
xsltproc xslt/strip-properties.xsl 249-2/onto.rdf > 257-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 249-2/refalign.rdf > 257-2/refalign.rdf
\rm -rf 257-4
mkdir 257-4
xsltproc xslt/strip-properties.xsl 249-4/onto.rdf > 257-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 249-4/refalign.rdf > 257-4/refalign.rdf
\rm -rf 257-6
mkdir 257-6
xsltproc xslt/strip-properties.xsl 249-6/onto.rdf > 257-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 249-6/refalign.rdf > 257-6/refalign.rdf
\rm -rf 257-8
mkdir 257-8
xsltproc xslt/strip-properties.xsl 249-8/onto.rdf > 257-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 249-8/refalign.rdf > 257-8/refalign.rdf

#####################################################################
# 258) Systematic: No names, No instances, flattened classes

#DONE
\rm -rf 258
mkdir 258
xsltproc xslt/strip-instances.xsl 251/onto.rdf > 258/onto.rdf
cp 251/refalign.rdf 258/refalign.rdf
\rm -rf 258-2
mkdir 258-2
xsltproc xslt/strip-instances.xsl 251-2/onto.rdf > 258-2/onto.rdf
cp 251-2/refalign.rdf 258-2/refalign.rdf
\rm -rf 258-4
mkdir 258-4
xsltproc xslt/strip-instances.xsl 251-4/onto.rdf > 258-4/onto.rdf
cp 251-4/refalign.rdf 258-4/refalign.rdf
\rm -rf 258-6
mkdir 258-6
xsltproc xslt/strip-instances.xsl 251-6/onto.rdf > 258-6/onto.rdf
cp 251-6/refalign.rdf 258-6/refalign.rdf
\rm -rf 258-8
mkdir 258-8
xsltproc xslt/strip-instances.xsl 251-8/onto.rdf > 258-8/onto.rdf
cp 251-8/refalign.rdf 258-8/refalign.rdf

#####################################################################
# 259) Systematic: No names, No instances, expanded classes

#DONE
\rm -rf 259
mkdir 259
xsltproc xslt/strip-instances.xsl 252/onto.rdf > 259/onto.rdf
cp 252/refalign.rdf 259/refalign.rdf
\rm -rf 259-2
mkdir 259-2
xsltproc xslt/strip-instances.xsl 252-2/onto.rdf > 259-2/onto.rdf
cp 252-2/refalign.rdf 259-2/refalign.rdf
\rm -rf 259-4
mkdir 259-4
xsltproc xslt/strip-instances.xsl 252-4/onto.rdf > 259-4/onto.rdf
cp 252-4/refalign.rdf 259-4/refalign.rdf
\rm -rf 259-6
mkdir 259-6
xsltproc xslt/strip-instances.xsl 252-6/onto.rdf > 259-6/onto.rdf
cp 252-6/refalign.rdf 259-6/refalign.rdf
\rm -rf 259-8
mkdir 259-8
xsltproc xslt/strip-instances.xsl 252-8/onto.rdf > 259-8/onto.rdf
cp 252-8/refalign.rdf 259-8/refalign.rdf

#####################################################################
# 260) Systematic: No names, No properties, flattened classes

#DONE
\rm -rf 260
mkdir 260
xsltproc xslt/strip-properties.xsl 251/onto.rdf > 260/onto.rdf
xsltproc xslt/strip-propalign.xsl 251/refalign.rdf > 260/refalign.rdf
\rm -rf 260-2
mkdir 260-2
xsltproc xslt/strip-properties.xsl 251-2/onto.rdf > 260-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 251-2/refalign.rdf > 260-2/refalign.rdf
\rm -rf 260-4
mkdir 260-4
xsltproc xslt/strip-properties.xsl 251-4/onto.rdf > 260-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 251-4/refalign.rdf > 260-4/refalign.rdf
\rm -rf 260-6
mkdir 260-6
xsltproc xslt/strip-properties.xsl 251-6/onto.rdf > 260-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 251-6/refalign.rdf > 260-6/refalign.rdf
\rm -rf 260-8
mkdir 260-8
xsltproc xslt/strip-properties.xsl 251-8/onto.rdf > 260-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 251-8/refalign.rdf > 260-8/refalign.rdf

#####################################################################
# 261) Systematic: No names, No properties, expanded classes

#DONE
\rm -rf 261
mkdir 261
xsltproc xslt/strip-properties.xsl 252/onto.rdf > 261/onto.rdf
xsltproc xslt/strip-propalign.xsl 252/refalign.rdf > 261/refalign.rdf
\rm -rf 261-2
mkdir 261-2
xsltproc xslt/strip-properties.xsl 252-2/onto.rdf > 261-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 252-2/refalign.rdf > 261-2/refalign.rdf
\rm -rf 261-4
mkdir 261-4
xsltproc xslt/strip-properties.xsl 252-4/onto.rdf > 261-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 252-4/refalign.rdf > 261-4/refalign.rdf
\rm -rf 261-6
mkdir 261-6
xsltproc xslt/strip-properties.xsl 252-6/onto.rdf > 261-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 252-6/refalign.rdf > 261-6/refalign.rdf
\rm -rf 261-8
mkdir 261-8
xsltproc xslt/strip-properties.xsl 252-8/onto.rdf > 261-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 252-8/refalign.rdf > 261-8/refalign.rdf

#####################################################################
# 262) Systematic: No names, no instances, no properties

#DONE
\rm -rf 262
mkdir 262
xsltproc xslt/strip-properties.xsl 253/onto.rdf > 262/onto.rdf
xsltproc xslt/strip-propalign.xsl 253/refalign.rdf > 262/refalign.rdf
\rm -rf 262-2
mkdir 262-2
xsltproc xslt/strip-properties.xsl 253-2/onto.rdf > 262-2/onto.rdf
xsltproc xslt/strip-propalign.xsl 253-2/refalign.rdf > 262-2/refalign.rdf
\rm -rf 262-4
mkdir 262-4
xsltproc xslt/strip-properties.xsl 253-4/onto.rdf > 262-4/onto.rdf
xsltproc xslt/strip-propalign.xsl 253-4/refalign.rdf > 262-4/refalign.rdf
\rm -rf 262-6
mkdir 262-6
xsltproc xslt/strip-properties.xsl 253-6/onto.rdf > 262-6/onto.rdf
xsltproc xslt/strip-propalign.xsl 253-6/refalign.rdf > 262-6/refalign.rdf
\rm -rf 262-8
mkdir 262-8
xsltproc xslt/strip-properties.xsl 253-8/onto.rdf > 262-8/onto.rdf
xsltproc xslt/strip-propalign.xsl 253-8/refalign.rdf > 262-8/refalign.rdf

#####################################################################
# 263) Systematic: No names, No instances, No hierarchy, flattened classes

#DONE
#\rm -rf 263
#mkdir 263
#xsltproc xslt/strip-instances.xsl 255/onto.rdf > 263/onto.rdf
#cp 255/refalign.rdf 263/refalign.rdf

#####################################################################
# 264) Systematic: No names, No instances, No hierarchy, expanded classes

#DONE
#\rm -rf 264
#mkdir 264
#xsltproc xslt/strip-instances.xsl 256/onto.rdf > 264/onto.rdf
#cp 256/refalign.rdf 264/refalign.rdf

#####################################################################
# 265) Systematic: No names, No instances, No hierarchy, flattened classes

#DONE
\rm -rf 265
mkdir 265
xsltproc xslt/strip-instances.xsl 260/onto.rdf > 265/onto.rdf
cp 260/refalign.rdf 265/refalign.rdf

#####################################################################
# 266) Systematic: No names, No instances, No hierarchy, expanded classes

#DONE
\rm -rf 266
mkdir 266
xsltproc xslt/strip-instances.xsl 261/onto.rdf > 266/onto.rdf
cp 261/refalign.rdf 266/refalign.rdf

#####################################################################
# 267) Systematic: Nothing, flattened classes

#DONE
#\rm -rf 267
#mkdir 267
#xsltproc xslt/strip-subclass.xsl 265/onto.rdf > 267/onto.rdf
#cp 265/refalign.rdf 267/refalign.rdf

#####################################################################
# 268) Systematic: Nothing, expanded classes

#DONE
#\rm -rf 268
#mkdir 268
#xsltproc xslt/strip-subclass.xsl 266/onto.rdf > 268/onto.rdf
#cp 266/refalign.rdf 268/refalign.rdf

#####################################################################
# //pass2: fix URI
# DONE!
# Has NOT become obsolete in 2009 (we stop providing lib) but for the 3xx

echo -n "Fixing URI "

for i in `ls -d [1-2][0-9][0-9]*` 
do
echo -n "*"$i"*"
if [ -f $i/refalign.rdf ]
then
ed -s $i/onto.rdf << EOF &>/dev/null
1,$ s;/tests/101/onto.rdf;/tests/$i/onto.rdf;g
w
EOF
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s;<uri2>http://oaei.ontologymatching.org/tests/101/onto.rdf</uri2>;<uri2>http://oaei.ontologymatching.org/tests/$i/onto.rdf</uri2>;g
w
EOF
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s;<onto2>file:///Web/html/oaei/tests/101/onto.rdf</onto2>;<onto2>file:///Web/html/oaei/tests/$i/onto.rdf</onto2>;g
w
EOF
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s;entity2 rdf:resource=\(['"]\)http://oaei.ontologymatching.org/tests/101/;entity2 rdf:resource=\1http://oaei.ontologymatching.org/tests/$i/;g
w
EOF
fi
if [ -f $i/refalign-iso8859.rdf ]
then
ed -s $i/onto-iso8859.rdf << EOF &>/dev/null
1,$ s;/tests/101/onto.rdf;/tests/$i/onto.rdf;g
w
EOF
ed -s $i/refalign-iso8859.rdf << EOF &>/dev/null
1,$ s;<uri2>http://oaei.ontologymatching.org/tests/101/onto.rdf</uri2>;<uri2>http://oaei.ontologymatching.org/tests/$i/onto.rdf</uri2>;g
w
EOF
# Beware, this is different here...
ed -s $i/refalign-iso8859.rdf << EOF &>/dev/null
1,$ s;<onto2>file:///Web/html/oaei/tests/101/onto.rdf</onto2>;<onto2>file:///Web/html/oaei/tests/$i/onto-iso8859.rdf</onto2>;g
w
EOF
ed -s $i/refalign-iso8859.rdf << EOF &>/dev/null
1,$ s;entity2 rdf:resource=\(['"]\)http://oaei.ontologymatching.org/tests/101/;entity2 rdf:resource=\1http://oaei.ontologymatching.org/tests/$i/;g
w
EOF
fi
done

for i in `ls -d [1-2][0-9][0-9]*` 
do
ed -s $i/onto.rdf << EOF &>/dev/null
1,$ s;oaei.ontologymatching.org/tests/;oaei.ontologymatching.org/$CURRENT/;g
w
EOF
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s;oaei.ontologymatching.org/tests/;oaei.ontologymatching.org/$CURRENT/;g
w
EOF
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s;file:///Web/html/oaei/tests/;http://oaei.ontologymatching.org/$CURRENT/;g
w
EOF
if [ -f $i/onto-iso8859.rdf ]
then
ed -s $i/onto-iso8859.rdf << EOF &>/dev/null
1,$ s;oaei.ontologymatching.org/tests/;oaei.ontologymatching.org/$CURRENT/;g
w
EOF
ed -s $i/refalign-iso8859.rdf << EOF &>/dev/null
1,$ s;oaei.ontologymatching.org/tests/;oaei.ontologymatching.org/$CURRENT/;g
w
EOF
ed -s $i/refalign-iso8859.rdf << EOF &>/dev/null
1,$ s;file:///Web/html/oaei/tests/;http://oaei.ontologymatching.org/$CURRENT/;g
w
EOF
fi
done

# The only changes that have to be made here, and only the first time
# tests are gererated in a campaign is:
# for 301-304
# edit onto.rdf and refalign.rdf
# change yyyy/ -> yyyy+1/ or tests/ /yyyy/benchmarks
# sh make.sh a last time

for i in `ls -d 30[1-4]`
do
echo -n "*"$i"*"
ed -s $i/refalign.rdf << EOF &>/dev/null
1,$ s:tests:2011/benchmark:g
w
EOF
ed -s $i/onto.rdf << EOF &>/dev/null
1,$ s:tests:2011/benchmark:g
w
EOF
done

#####################################################################
# //pass3: generate HTML
# DONE!

echo
echo -n "Generating HTML "

for i in `ls -d [0-9][0-9][0-9]*` 
do
echo -n "*"$i"*"
xsltproc xslt/form-align.xsl $i/refalign.rdf > $i/refalign.html
xsltproc xslt/owl2html.xsl $i/onto.rdf > $i/onto.html
done

echo

#####################################################################
# //pass4: generate ZIP file
# DONE!

echo "Be sure that CURRENT was correctly set ("$CURRENT")"
echo "Apply pass4 manually with the correct VERSION ("$VERSION")"
echo "Check that the 3xx series is in the appropriate form (files contain correct URI)"

exit

# BEWARE: THIS LAST PART CANNOT BE BLINDLY APPLIED
# 2010: should be roughly correct
# copy

cd ..

RELEASEDIR=/Web/html/oaei

### BEWARE, THIS MAY BE A PROBLEM WITH GIT !!!!
\rm -rf $RELEASEDIR/$CURRENT.old
mv $RELEASEDIR/$CURRENT $RELEASEDIR/$CURRENT.old

cp -rf benchmarks $RELEASEDIR/$CURRENT

cd $RELEASEDIR/$CURRENT
/bin/rm bench.zip
cd ..
zip bench$VERSION.zip -r  benchmarks/ -x benchmarks/RCS/* benchmarks/xslt/RCS/* benchmarks/NEW305/*
mv bench$VERSION.zip $RELEASEDIR/versions
cp $RELEASEDIR/versions/bench$VERSION.zip benchmarks/bench.zip


