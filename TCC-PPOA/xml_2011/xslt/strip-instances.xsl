<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id: strip-instances.xsl,v 1.9 2011/05/31 13:46:42 euzenat Exp $ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
		xmlns:my="http://oaei.ontologymatching.org/tests/101/onto.rdf#"
		xmlns:units="http://visus.mit.edu/fontomri/0.01/units.owl#" 
		xmlns:foaf="http://xmlns.com/foaf/0.1/" 
		xmlns:ical="http://www.w3.org/2002/12/cal/ical#" 
		xmlns:xsd="http://www.w3.org/2001/XMLSchema#" 
		xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
		xmlns:owl="http://www.w3.org/2002/07/owl#" 
		xmlns:wot="http://xmlns.com/wot/0.1/" 
		xmlns:dc="http://purl.org/dc/elements/1.1/" 
		xmlns:dcterms="http://purl.org/dc/terms/" 
		xmlns:dctype="http://purl.org/dc/dcmitype/"
		version="1.0"
>
  <xsl:param name="param"></xsl:param>
  <xsl:param name="value"></xsl:param>
  <xsl:param name="ref"></xsl:param>

  <xsl:output method="xml" encoding="iso-8859-1"/>

  <xsl:template match="foaf:Person"/>
  <xsl:template match="my:Journal|my:Publisher|my:Conference|my:Workshop"/>
  <xsl:template match="my:Proceedings|my:InProceedings|my:Article|my:InBook|my:Misc|my:Monograph"/>
  <xsl:template match="owl:oneOf"/>

  <!-- JE added 2008 for scrambled instances -->
  <xsl:template match="my:qsdsquj|my:zauio|my:zqedzbx|my:izVyrw"/>
  <xsl:template match="my:zdqssqdb|my:deqdxcsqcsq|my:hazdn|my:vccfsq|my:wxsdxqsdh|my:dzajndsq"/>

  <xsl:template match="@*|node()"> <!-- node() = text()|* ? -->
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
	
</xsl:stylesheet>
